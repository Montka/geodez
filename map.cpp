#define M_PI 3.14159265358 /* pi */
#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "map.h"

const long double Map::R=6371000;
const long double Map::G=980000;

Map::Map(sqrIntArray dg, long double cornerB, long double cornerL ):
    dgMatrix(dg),
    B_c(cornerB),
    L_c(cornerL),
    lines(0),
    columns(0)
{
    lines=dgMatrix.size();

    for(int i=0; i<lines; ++i)
        columns=(dgMatrix[i].size()>columns?dgMatrix[i].size():columns);

    matrix=new Cell**[lines];
    for (int i=0; i<lines; ++i)
        matrix[i]=new Cell* [columns];

    setCenter();

    for(int i=0; i<lines;++i)
        for(int j=0; j<columns; ++j)
            if(!isImportant(fabs(B_c-i),L_c-j))
            matrix[i][j]=new CellRegular(fabs(B_c-i),L_c-j,B_t,L_t,dgMatrix[i][j]);
            else
            matrix[i][j]=new CellHighPrecision(fabs(B_c-i),L_c-j,B_t,L_t,dgMatrix[i][j]);

}

Map::~Map()
{
    for(int i=0; i<lines;++i)
        for(int j=0; j<columns;++j)
            delete matrix[i][j];

    for(int i=0; i<lines;++i)
        delete [] matrix[i];

    delete [] matrix;
}

void Map::init()
{
for(int i=0; i<lines;++i)
    for(int j=0; j<columns;++j)
        matrix[i][j]->init();

}



void Map::dump(const std::string &path)
{
    std::ofstream file(path.c_str());
    if(file.is_open())
    file<<"B;L;B(rads);L(rads);Quarter;dOmega;Psi;A;S;S';dg;Xi;N;Dzeta"<<std::endl;
    for(int i=0; i<lines;++i)
        for(int j=0; j<columns;++j)
        {//std::cout<<matrix[i][j]->dump()<<std::endl;
         file<<B_c-i<<";"<<L_c-j<<";"<<matrix[i][j]->dump();
        }
    file<<std::setprecision(PRECISION);
    file<<std::endl<<"Xi:;"<<Xi()<<std::endl;
    file<<"N:;"<<N()<<std::endl;
    file<<"Dzeta:;"<<Dzeta()<<std::endl;
    file.close();
}

long double Map::Xi()
{
    long double sum=0;
    for(int i=0; i<lines;++i)
        for(int j=0; j<columns;++j)
            if(isnormal(matrix[i][j]->getXi()))
            sum+=matrix[i][j]->getXi();

    return (1/(4*M_PI*G))*sum*206265;

}

long double Map::N()
{
    long double sum=0;
    for(int i=0; i<lines;++i)
        for(int j=0; j<columns;++j)
            if(isnormal(matrix[i][j]->getN()))
            sum+=matrix[i][j]->getN();

   return (1/(4*M_PI*G))*sum*206265;

}
long double Map::Dzeta()
{
    long double sum=0;
    for(int i=0; i<lines;++i)
        for(int j=0; j<columns;++j)
            if(isnormal(matrix[i][j]->getDzeta()))
            sum+=matrix[i][j]->getDzeta();

    return (R/(4*M_PI*G))*sum;

}

bool Map::setCenter()
{
    if(lines%2!=0) B_t=B_c-floor(lines/2);
    else {
        std::cout<<std::endl<<"Center is undefined, select latitude:"<<std::endl;
        B_t=_select(B_c-lines/2+1,B_c-lines/2);
    }

    if(columns%2!=0) L_t=L_c-floor(columns/2);
    else {
        std::cout<<std::endl<<" Center is undefined, select longitude:"<<std::endl;
        L_t=_select(L_c-columns/2+1,L_c-columns/2);
    }
    std::cout<<"CENTER:"<<L_t<<" "<<B_t;
    return true;
}
long double Map::_select(long double first, long double second)
{
    int choice;
    std::cout<<"1) "<<first<<std::endl<<"2) "<<second<<std::endl;
    while (true) {
        std::cin>>choice;
        switch (choice) {
        case 1:
            return first;
        case 2:
            return second;
        default:
            std::cout<<"1 or 2 avalible"<<std::endl;
            break;
        }
    }
}

bool Map::isImportant(long double b, long double l)
{
    if((abs(B_t-b)>1 || abs(L_t-l)>1) || (B_t==b && L_t==l)) return false;
    else return true;
}
