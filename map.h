#ifndef MAP_H
#define MAP_H
#include <vector>

class Cell;

#include "cell.h"

typedef std::vector<std::vector<double> > sqrIntArray;

class Map
{
Cell*** matrix;
sqrIntArray dgMatrix;
long double B_c, L_c;
long double B_t,L_t;
int lines,columns;
static const long double R;
static const long double G;
public:
Map(sqrIntArray dg, long double cornerB, long double cornerL);
~Map();

void init();
void dump(const std::string &path);
long double Xi();
long double N();
long double Dzeta();
private:
bool setCenter();
long double _select(long double first, long double second);
bool isImportant(long double b, long double l);
};
#endif
