#ifndef CELL_H
#define CELL_H

#define PRECISION 50
#define M_PI 3.1415926535897932384626433832795028841971693993751L /* pi */

#include <string>
class Map;
#include "map.h"
#include "quarter.h"
class Cell
{
protected:
 long double B_t, L_t;
 long double B_t_deg, L_t_deg;
 long double Xi, N, Dzeta;
public:
    virtual void init()=0;
    virtual std::string dump()=0;
    long double getXi() const;
    long double getN() const;
     long double getDzeta() const;
protected:
    long double toRads(long double angle) const;
    long double toDegs(long double angle) const;
    Quarter getQuater(long double B, long double L);

    long double _omega(long double b) const;
    long double _psi(long double b, long double l) const;
    long double _A(long double b, long double l, Quarter qa) const;
    long double _S(long double psi) const;
    long double _S_(long double psi) const;
    long double _xi(long double dg, long double s_, long double a, long double domega) const;
    long double _n(long double dg, long double s_, long double a, long double domega) const;
    long double _dzeta(long double dg, long double s_, long double domega) const;
};
class CellRegular:public Cell
{
private:

    Quarter qa;
    long double B, L; //latitude, longitude
    long double dOmega;
    long double Psi;
    long double A; //Ai in dgs with rumb
    long double S, S_;
    long double dG;


public:
    CellRegular(long double _B, long double _L,long double _B_t,long double _L_t, long double _dG);
    virtual void init();
    virtual std::string dump();

};

/*
 *  0 | 1
 * --------
 *  3 | 2
 *
 */
class CellHighPrecision: public Cell
{
    Quarter qa[5];
    long double B[5], L[5];
    long double dOmega[5];
    long double Psi[5];
    long double A[5]; //Ai in dgs with rumb
    long double S[5], S_[5];
    long double dG;

    long double SAvg;
    long double S_Avg;
public:
    CellHighPrecision(long double _B, long double _L,long double _B_t,long double _L_t, long double _dG);
    virtual void init();
    virtual std::string dump();
};
#endif
