#include <iostream>
#include <fstream>
#include <stdexcept>

#include "parser.h"

typedef std::vector<std::vector<double> > sqrIntArray;

Parser::Parser(){}

bool Parser::parse(const std::string &path)
{
    std::ifstream file(path.c_str());
    if(!file.is_open()) return false;

    std::string line;
    try{
        std::getline(file,line);
        B_init=std::stod(line);
        std::getline(file,line);
        L_init=std::stod(line);
        while(std::getline(file,line)) {
            dgMatrix.push_back(parse_line(line));
        }
    }
    catch (const std::invalid_argument& e)
    {
        return false;
    }
    catch (const std::out_of_range& e)
    {
        return false;
    }
    file.close();
    for(int i=0; i<dgMatrix.size(); i++)
    {
        for(int j=0; j<dgMatrix[i].size(); j++)
            std::cout<<dgMatrix[i][j]<<" ";
        std::cout<<std::endl;
    }
    std::cout<<B_init<<" "<<L_init;
    return true;
}

std::vector<double> Parser::parse_line(std::string str)
{
    std::vector<double> res;
    std::string copy_str;
    for (int i=0; i<str.size(); i++)
    {
        if (str[i]==' ')
        {
            if (!copy_str.empty())
            {
                res.push_back(std::stod(copy_str));
                copy_str.clear();
            }
            continue;
        }
        copy_str += str[i];

    }
    if (!copy_str.empty())
    {

        res.push_back(std::stod(copy_str));
        copy_str.clear();
    }
    return res;
}

Map* Parser::createMap()
{
  return new Map(dgMatrix,B_init-0.5,L_init-0.5);
}
