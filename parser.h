#ifndef PARSER_H
#define PARSER_H

#include <string.h>
#include <vector>

#include "map.h"
typedef std::vector<std::vector<double> > sqrIntArray;
class Parser
{
private:
sqrIntArray dgMatrix;
long double B_init,L_init;

public:
Parser();
bool parse(const std::string &path);
Map* createMap();
private:
std::vector<double> parse_line(std::string str);
};
#endif
