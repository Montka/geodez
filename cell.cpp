#include <math.h>
#include <sstream>
#include <iomanip>
#include "cell.h"

long double Cell::getXi() const {return Xi;}
long double Cell::getN() const {return N;}
long double Cell::getDzeta() const {return Dzeta;}

long double Cell::toRads(long double angle) const
{
    return (angle* M_PI )/180;
    //return angle* 0.01745329252;
}

long double Cell::toDegs(long double angle) const
{
    return angle*(180/M_PI);
}

long double Cell::_omega(long double b) const
{return toRads(1)*toRads(1)*cos(b);}

long double Cell::_psi(long double b, long double l) const
{return acos(sin(Cell::B_t)*sin(b)+cos(Cell::B_t)*cos(b)*cos(l-Cell::L_t));}

long double Cell::_A(long double b, long double l, Quarter qa) const
{
long double A=toDegs( atan(fabs((cos(Cell::B_t)*sin(l-Cell::L_t))/(cos(Cell::B_t)*sin(b) - sin(Cell::B_t)*cos(b)*cos(l-Cell::L_t)))));
long double ret;
switch(qa)
{
case(I):
	ret=A;
	break;
case(II):
	ret=180-A;
	break;
case(III):
	ret=180+A;
	break;
case(IV):
	ret=360-A;
	break;
}
return ret;
}

long double Cell::_S(long double psi) const
{return (1/sin(psi/2.))+1-6*sin(psi/2.)-5*cos(psi)-3*cos(psi)*log(sin(psi/2.)+pow(sin(psi/2.),2));}

long double Cell::_S_(long double psi) const
{return 5*sin(psi)-3*cos(psi/2)+3*sin(psi)*log(pow(sin(psi/2),2)+sin(psi/2)) - ((3*cos(psi)*(0.5*cos(psi/2)+sin(psi/2)*cos(psi/2)))/
            (pow(sin(psi/2),2)+sin(psi/2)))- 0.5*(cos(psi/2)/sin(psi/2))*(1/sin(psi/2));}

long double Cell::_xi(long double dg, long double s_, long double a, long double domega) const
{return dg*s_*cos(toRads(a))*domega;}

long double Cell::_n(long double dg, long double s_, long double a, long double domega) const
{return dg*s_*sin(toRads(a))*domega;}

long double Cell::_dzeta(long double dg, long double s, long double domega) const
{return dg*s*domega;}

Quarter Cell::getQuater(long double B, long double L)
{

    if(B>Cell::B_t_deg && L>Cell::L_t_deg) return IV;
    if(B>Cell::B_t_deg && L<=Cell::L_t_deg) return I;
    if(B<Cell::B_t_deg && L>Cell::L_t_deg) return III;
    if(B<Cell::B_t_deg && L<=Cell::L_t_deg) return II;

    if(B>=0) //B==B_t
    {
        if(L>Cell::L_t_deg) return IV;
        if(L<=Cell::L_t_deg) return I;
    }
    else
    {
        if(L>Cell::L_t_deg) return II;
        if(L<=Cell::L_t_deg) return III;
    }
    //return I;
}


CellRegular::CellRegular(long double _B, long double _L, long double _B_t, long double _L_t, long double _dG):
    dG(_dG)
{
    B=toRads(_B);
    L=toRads(_L);
    Cell::B_t_deg=_B_t;
    Cell::L_t_deg=_L_t;
    Cell::B_t=toRads(_B_t);
    Cell::L_t=toRads(_L_t);
    qa=getQuater(_B,_L);


}

void CellRegular::init()
{
   dOmega=_omega(B);
   Psi=_psi(B,L);
   A=_A(B,L,qa);
   S=_S(Psi);
   S_=_S_(Psi);
   Xi=_xi(dG,S_,A,dOmega);
   N=_n(dG,S_,A,dOmega);
   Dzeta=_dzeta(dG,S,dOmega);
}

std::string CellRegular::dump()
{
std::stringstream ret;
ret<<std::setprecision(PRECISION)<<B<<";"<<L<<";"<<qa+1<<";"<<dOmega<<";"<<Psi<<";"<<A<<";"<<S<<";"<<S_<<";"<<dG<<";"<<Xi<<";"<<N<<";"<<Dzeta<<"\n";
return ret.str();
}


/*
 *  0 | 1
 * --------
 *  3 | 2
 *
 */
CellHighPrecision::CellHighPrecision(long double _B, long double _L, long double _B_t, long double _L_t, long double _dG):
    dG(_dG)
{
    Cell::B_t=toRads(_B_t);
    Cell::L_t=toRads(_L_t);
    Cell::B_t_deg=_B_t;
    Cell::L_t_deg=_L_t;

    B[0]=B[1]=toRads(_B+0.25);
    B[2]=B[3]=toRads(_B-0.25);
    L[0]=L[3]=toRads(_L+0.25);
    L[1]=L[2]=toRads(_L-0.25);
    L[4]=toRads(_L);
    B[4]=toRads(_B);

    qa[0]=getQuater(_B+0.25,_L+0.25);
    qa[1]=getQuater(_B+0.25,_L-0.25);
    qa[2]=getQuater(_B-0.25,_L-0.25);
    qa[3]=getQuater(_B-0.25,_L+0.25);
    qa[4]=getQuater(_B,_L);
}

void CellHighPrecision::init()
{
  for(int i=0; i<=4; ++i)
  {
      dOmega[i]=_omega(B[i]);
      Psi[i]=_psi(B[i],L[i]);
      A[i]=_A(B[i],L[i],qa[i]);
      S[i]=_S(Psi[i]);
      S_[i]=_S_(Psi[i]);
  }
  for(int i=0;i<=4;++i)
  {
      SAvg+=S[i];
      S_Avg+=S_[i];
  }
  SAvg/=5;
  S_Avg/=5;

  Xi=_xi(dG,S_Avg,A[4],dOmega[4]);
  N=_n(dG,S_Avg,A[4],dOmega[4]);
  Dzeta=_dzeta(dG,S[4],dOmega[4]);
}

std::string CellHighPrecision::dump()
{
    std::stringstream ret;
    ret<<std::setprecision(PRECISION)<<B[4]<<";"<<L[4]<<";"<<qa[4]+1<<";"<<dOmega[4]<<";"<<Psi[4]<<";"<<A[4]<<";"<<SAvg<<";"<<S_Avg<<";"<<dG<<";"<<Xi<<";"<<N<<";"<<Dzeta<<"\n";
    for (int i=0; i<4;++i)
        ret<<std::setprecision(PRECISION)<<";;"<<B[i]<<";"<<L[i]<<";"<<qa[i]+1<<";"<<dOmega[i]<<";"<<Psi[i]<<";"<<A[i]<<"\n";


    return ret.str();
}
