TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG +=c++11
QMAKE_LFLAGS += -static -static-libgcc -static-libstdc++ -lpthread

SOURCES += main.cpp \
    cell.cpp \
    map.cpp \
    parser.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    cell.h \
    map.h \
    parser.h \
    quarter.h

