#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>

#include "quarter.h"
#include "map.h"
#include "parser.h"



int main()
{
Parser parser;
std::string path;
std::cout<<"Enter path .map file: ";
std::cin>>path;

if(parser.parse(path))
{
   std::cout<<std::endl<<"File sucsesfully parsed."<<std::endl;
   Map* map=parser.createMap();
   std::cout<<std::endl<<"Map created."<<std::endl;
map->init();
std::cout<<"Map inited."<<std::endl;
map->dump("result.csv");
std::cout<<"Results saved."<<std::endl;
delete map;
}
else
    std::cout<<"Error occured during map parsing. Check file existence";

return 0;
}
